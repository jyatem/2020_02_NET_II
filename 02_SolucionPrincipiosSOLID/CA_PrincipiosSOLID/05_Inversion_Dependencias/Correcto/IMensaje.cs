﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._05_Inversion_Dependencias.Correcto
{
    public interface IMensaje
    {
        void EnviarMensaje();
    }
}
