﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._05_Inversion_Dependencias.Correcto
{
    public class Notificacion
    {
        private IMensaje _mensaje;

        public IMensaje Mensaje
        {
            set
            {
                _mensaje = value;
            }
        }

        public Notificacion()
        {
        }

        public Notificacion(IMensaje mensaje)
        {
            _mensaje = mensaje;
        }

        public void EnviarNotificacion()
        {
            _mensaje.EnviarMensaje();
        }
    }
}
