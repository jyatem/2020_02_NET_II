﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._05_Inversion_Dependencias.Error
{
    public class Notificacion
    {
        private Email _email;
        private SMS _sms;

        public Notificacion()
        {
            _email = new Email();
            _sms = new SMS();
        }

        public void EnviarNotificacion(char tipo)
        {
            switch (tipo)
            {
                case 'E':
                    _email.EnviarEMail();
                    break;
                case 'S':
                    _sms.EnviarSMS();
                    break;
                default:
                    break;
            }
            
        }
    }
}
