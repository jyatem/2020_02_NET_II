﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._01_Unica_Responsabilidad.Error
{
    public class Empleado
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public bool Insertar()
        {
            return true;
        }

        public void GenerarReporte()
        {
        }
    }
}
