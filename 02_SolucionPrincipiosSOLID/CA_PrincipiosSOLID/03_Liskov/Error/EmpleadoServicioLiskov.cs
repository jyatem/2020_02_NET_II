﻿using CA_PrincipiosSOLID._02_Abierto_Cerrado.Correcto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._03_Liskov.Error
{
    public class EmpleadoServicioLiskov
    {
        public List<Empleado> Empleados { get; set; }

        public void CalcularBono()
        {
            foreach (var empleado in Empleados)
            {
                if (empleado is Gerente)
                {
                    empleado.Bono = empleado.Sueldo * 6; // Está cambiando el comportamiento
                }
            }
        }
    }
}
