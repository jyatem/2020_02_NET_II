﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._04_Segregacion_Interfaces.Real.Error
{
    public interface IPersistir
    {
        void Guardar(ReservaDetalle reservaDetalle);
        void EnviarNotificacion(ReservaDetalle reservaDetalle);
    }
}
