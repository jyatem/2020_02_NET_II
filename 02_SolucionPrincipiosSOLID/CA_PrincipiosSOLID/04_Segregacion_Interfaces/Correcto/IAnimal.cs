﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._04_Segregacion_Interfaces.Correcto
{
    public interface IAnimal
    {
        void Alimentar();
    }
}
