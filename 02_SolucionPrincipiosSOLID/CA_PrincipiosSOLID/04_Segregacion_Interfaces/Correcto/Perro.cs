﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._04_Segregacion_Interfaces.Correcto
{
    public class Perro : IAnimal, IDomestico
    {
        public void Acariciar()
        {
            throw new NotImplementedException();
        }

        public void Alimentar()
        {
            throw new NotImplementedException();
        }
    }
}
