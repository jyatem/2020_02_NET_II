﻿using CA_PrincipiosSOLID._05_Inversion_Dependencias.Correcto;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            IMensaje email = new EMail();

            Notificacion notificacion = new Notificacion(email);
            notificacion.EnviarNotificacion();

            IMensaje sms = new SMS();
            notificacion.Mensaje = sms;
            notificacion.EnviarNotificacion();
        }
    }
}
