﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._02_Abierto_Cerrado.Correcto
{
    public class EmpleadoServicio
    {
        public List<Empleado> Empleados { get; set; }

        public void CalcularBono()
        {
            foreach (var empleado in Empleados)
            {
                empleado.CalcularBono();
            }
        }
    }
}
