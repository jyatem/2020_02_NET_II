﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._02_Abierto_Cerrado.Correcto
{
    public class Programador : Empleado
    {
        public override void CalcularBono()
        {
            Bono = Sueldo * 10;
        }
    }
}
