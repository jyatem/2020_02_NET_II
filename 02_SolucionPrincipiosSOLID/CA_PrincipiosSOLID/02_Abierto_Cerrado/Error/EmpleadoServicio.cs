﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._02_Abierto_Cerrado.Error
{
    public class EmpleadoServicio
    {
        public List<Empleado> Empleados { get; set; }

        public void CalcularBono()
        {
            foreach (var empleado in Empleados)
            {
                double bono = 0;

                switch (empleado.Tipo)
                {
                    case TipoEmpleado.Programador:
                        bono = empleado.Sueldo * 10;
                        break;
                    case TipoEmpleado.Gerente:
                        bono = empleado.Sueldo * 5;
                        break;
                    case TipoEmpleado.Tester:
                        bono = empleado.Sueldo * 15;
                        break;
                    default:
                        break;
                }

                empleado.Bono = bono;
            }
        }
    }
}
