﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_PrincipiosSOLID._02_Abierto_Cerrado.Error
{
    public class Empleado
    {
        public string Nombre { get; set; }

        public double Sueldo { get; set; }

        public double Bono { get; set; }

        public TipoEmpleado Tipo { get; set; }
    }
}
