﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Linq
{
    public class Ciudad
    {
        public int CiudadId { get; set; }

        public string NombreCiudad { get; set; }
    }
}
