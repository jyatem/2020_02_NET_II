﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Linq
{
    class Program
    {
        delegate bool BuscarEstudiante(Estudiante estudiante);

        class ExtensionEstudiante
        {
            public static Estudiante[] Where(Estudiante[] arregloEstudiantes, BuscarEstudiante del)
            {
                int i = 0;

                Estudiante[] resultado = new Estudiante[20];

                foreach (Estudiante estudiante in arregloEstudiantes)
                {
                    if (del(estudiante))
                    {
                        resultado[i] = estudiante;
                        i++;
                    }
                }

                return resultado;
            }
        }

        static void Main(string[] args)
        {
            Estudiante[] estudiantes =
            {
                new Estudiante {EstudianteId = 1, Nombre = "Jairo", Apellido = "Yate", Nota = 5, CiudadId = 5},
                new Estudiante {EstudianteId = 2, Nombre = "Lina", Apellido = "Mora", Nota = 3, CiudadId = 3},
                new Estudiante {EstudianteId = 3, Nombre = "Mariana", Apellido = "Yate", Nota = 2, CiudadId = 2},
                new Estudiante {EstudianteId = 4, Nombre = "Juan Manuel", Apellido = "Yate", Nota = 3, CiudadId = 1},
                new Estudiante {EstudianteId = 5, Nombre = "Raquel", Apellido = "Martínez", Nota = 4, CiudadId = 5},
                new Estudiante {EstudianteId = 6, Nombre = "Francisco", Apellido = "Marín", Nota = 2, CiudadId = 4}
            };

            List<Ciudad> ciudades = new List<Ciudad>()
            {
                new Ciudad {CiudadId = 1, NombreCiudad = "Medellin"},
                new Ciudad {CiudadId = 2, NombreCiudad = "Envigado"},
                new Ciudad {CiudadId = 3, NombreCiudad = "Itagui"},
                new Ciudad {CiudadId = 4, NombreCiudad = "Bello"},
                new Ciudad {CiudadId = 5, NombreCiudad = "Tuluá"}
            };

            #region [ C# 1.0 ]
            Estudiante[] estudiantesGanaronV1 = new Estudiante[10];

            int i = 0;

            foreach (Estudiante estudiante in estudiantes)
            {
                if (estudiante.Nota >= 3 && estudiante.Nota <= 5)
                {
                    estudiantesGanaronV1[i] = estudiante;
                    i++;
                }
            }
            #endregion

            #region [ C# 2.0 ]
            Estudiante[] estudiantesGanaronV2 = ExtensionEstudiante.Where(estudiantes, delegate (Estudiante estudiante)
            {
                return estudiante.Nota >= 3 && estudiante.Nota <= 5;
            });

            Estudiante[] estudiantesApellidoYate = ExtensionEstudiante.Where(estudiantes, delegate (Estudiante estudiante)
            {
                return estudiante.Apellido == "Yate";
            });
            #endregion

            #region [ C# 3.0 en adelante - Method ]
            Estudiante[] estudiantesGanaronV3 = estudiantes.Where(e => e.Nota >= 3 && e.Nota <= 5).ToArray();

            List<Estudiante> estudiantesApellidosYate2 = estudiantes.Where(e => e.Apellido == "Yate").ToList();
            #endregion

            #region [ C# 3.0 en adelante - Query ]
            var estudiantesGanaronQuery = from e in estudiantes
                                          where e.Nota >= 3 && e.Nota <= 5
                                          select e;
            #endregion

            #region [ Query con varios filtros ]
            string nombre = "a";
            string apellido = "";
            int nota = 0;

            var variosFiltros = from e in estudiantes
                                select e;

            if (nombre != "")
            {
                variosFiltros = variosFiltros.Where(e => e.Nombre.Contains(nombre));
            }

            if (apellido != "")
            {
                variosFiltros = variosFiltros.Where(e => e.Apellido.StartsWith(apellido));
            }

            if (nota != 0)
            {
                variosFiltros = variosFiltros.Where(e => e.Nota == nota);
            }

            //foreach (var item in variosFiltros)
            //{
            //    Console.WriteLine($"{item.Nombre} {item.Apellido}");
            //}
            #endregion

            #region [ Mostrando algunas propiedades del objeto ]
            var ciertasPropiedades = from e in estudiantes
                                     select new { NombreCompleto = e.Nombre + " " + e.Apellido, Nota = e.Nota};

            ciertasPropiedades = ciertasPropiedades.Where(e => e.Nota == 3);

            foreach (var item in ciertasPropiedades)
            {
                Console.WriteLine($"{item.NombreCompleto}");
            }
            #endregion

            #region [ Order by - Query ]
            var estOrderBy = from e in estudiantes
                             orderby e.Nombre
                             select e;

            var estOrderByDesc = from e in estudiantes
                             orderby e.Nombre descending
                             select e;
            #endregion

            #region [ Order by - Method ]
            var estOrderByMethod = estudiantes.OrderBy(e => e.Nombre);

            var estOrderByDescMethod = estudiantes.OrderByDescending(e => e.Nombre);
            #endregion

            #region [ Multiple Ordenamiento - Query ]
            var estOrdByMult = from e in estudiantes
                               orderby e.Nombre descending, e.Apellido
                               select e;
            #endregion

            #region [ Multiple Ordenamiento - Method ]
            var estOrderByMethodMult = estudiantes.OrderBy(e => e.Nombre).OrderByDescending(e => e.Apellido);

            var estOrderByMethodMultMismo = estudiantes.OrderBy(e => e.Nombre).ThenBy(e => e.Apellido);
            #endregion

            #region [ Agrupamiento - Query ]
            var estAgrupamientoNota = from e in estudiantes
                                      group e by e.Nota;

            Console.WriteLine("AGRUPAMIENTO QUERY");

            foreach (var notaGrupo in estAgrupamientoNota)
            {
                Console.WriteLine($"Grupo Nota: {notaGrupo.Key}");

                foreach (Estudiante estudiante in notaGrupo)
                {
                    Console.WriteLine($"Nombre: {estudiante.Nombre}");
                }
                Console.WriteLine();
            }
            #endregion

            #region [ Agrupamiento - Method ]
            var estAgrupamientoNotaMethod = estudiantes.GroupBy(e => e.Nota);
            var estAgrupamientoNotaMethodLookup = estudiantes.ToLookup(e => e.Nota);

            Console.WriteLine("AGRUPAMIENTO METHOD");

            foreach (var notaGrupo in estAgrupamientoNotaMethod)
            {
                Console.WriteLine($"Grupo Nota: {notaGrupo.Key}");

                foreach (Estudiante estudiante in notaGrupo)
                {
                    Console.WriteLine($"Nombre: {estudiante.Nombre}");
                }
                Console.WriteLine();
            }
            #endregion

            #region [ Máximo - Method ]
            var mayorNota = estudiantes.Max(e => e.Nota);

            Console.WriteLine("{0}Mayor nota: {1}", Environment.NewLine, mayorNota);
            #endregion

            #region [ Promedio - Method ]
            var promedio = estudiantes.Average(e => e.Nota);

            Console.WriteLine("{0}Promedio: {1}", Environment.NewLine, promedio);
            #endregion

            #region [ Contador - Method ]
            var cantidadApellidosYate = estudiantes.Count(e => e.Apellido == "Yate");

            Console.WriteLine("{0}Yates: {1}", Environment.NewLine, cantidadApellidosYate);
            #endregion

            #region [ Aggregate - Method ]
            string nombresSeparadosPorComa = estudiantes.Aggregate<Estudiante, string>("Estudiantes: ", (str, e) => str += e.Nombre + ",");

            Console.WriteLine("{0}{1}", Environment.NewLine, nombresSeparadosPorComa);

            // Sumar todas las notas con un Aggregate

            int sumaNotas = estudiantes.Aggregate<Estudiante, int>(0, (sumaNota, e) => sumaNota += e.Nota);

            Console.WriteLine("{0}{1}", Environment.NewLine, sumaNotas);
            #endregion

            #region [ Inner Join - Method ]
            var innerJoin = estudiantes.Join(ciudades, e => e.CiudadId, c => c.CiudadId, (estudiante, ciudad) => new { NombreEstudiante = estudiante.Nombre, NombreCiudad = ciudad.NombreCiudad });

            Console.WriteLine("INNER JOIN METHOD");

            foreach (var item in innerJoin)
            {
                Console.WriteLine($"{item.NombreEstudiante} de la ciudad {item.NombreCiudad}");
            }
            #endregion

            #region [ Inner Join - Query ]
            var innerJoinQuery = from e in estudiantes
                                 join c in ciudades
                                 on e.CiudadId equals c.CiudadId
                                 select new { NombreEstudiante = e.Nombre, NombreCiudad = c.NombreCiudad };

            Console.WriteLine("INNER JOIN QUERY");

            foreach (var item in innerJoinQuery)
            {
                Console.WriteLine($"{item.NombreEstudiante} de la ciudad {item.NombreCiudad}");
            }
            #endregion

            #region [ Skip - Method ]
            var skipList = estudiantes.Skip(2);

            Console.WriteLine();
            Console.WriteLine("SKIP");

            foreach (var item in skipList)
            {
                Console.WriteLine(item.Nombre);
            }
            #endregion

            #region [ Skip While - Method ]
            var skipListWhile = estudiantes.SkipWhile(e => e.Nota >= 3);

            Console.WriteLine();
            Console.WriteLine("SKIP WHILE");

            foreach (var item in skipListWhile)
            {
                Console.WriteLine(item.Nombre);
            }
            #endregion

            #region [ Take - Method ]
            var takeList = estudiantes.Take(3);

            Console.WriteLine();
            Console.WriteLine("TAKE");

            foreach (var item in takeList)
            {
                Console.WriteLine(item.Nombre);
            }
            #endregion

            #region [ Skip y Take ]
            var skipTakeList = estudiantes.Skip(2).Take(2);

            Console.WriteLine();
            Console.WriteLine("SKIP Y TAKE");

            foreach (var item in skipTakeList)
            {
                Console.WriteLine(item.Nombre);
            }
            #endregion

            Console.ReadLine();
        }
    }
}
