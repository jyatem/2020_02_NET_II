﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Linq
{
    public class Estudiante
    {
        public int EstudianteId { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public int Nota { get; set; }

        public int CiudadId { get; set; }
    }
}
