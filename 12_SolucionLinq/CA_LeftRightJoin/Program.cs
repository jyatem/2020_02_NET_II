﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LeftRightJoin
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Departamento> departamentos = new List<Departamento>
            {
                new Departamento {Id = 1, Nombre = "Desarrollo"},
                new Departamento {Id = 2, Nombre = "Pruebas"},
                new Departamento {Id = 3, Nombre = "Soporte"}
            };

            List<Empleado> empleados = new List<Empleado>
            { 
                new Empleado { Id = 1, Nombre = "A", DepartamentoId = 1},
                new Empleado { Id = 2, Nombre = "B", DepartamentoId = 2},
                new Empleado { Id = 3, Nombre = "C", DepartamentoId = 1},
                new Empleado { Id = 4, Nombre = "D", DepartamentoId = 4},
                new Empleado { Id = 5, Nombre = "E", DepartamentoId = 2},
                new Empleado { Id = 6, Nombre = "F", DepartamentoId = 4}
            };

            #region [ Left Join ]
            var leftJoin = from e in empleados
                           join d in departamentos
                           on e.DepartamentoId equals d.Id into enlaceEmpleadosDepartamentos
                           from d in enlaceEmpleadosDepartamentos.DefaultIfEmpty()
                           select new { NombreEmpleado = e.Nombre, NombreDepartamento = d != null ? d.Nombre : null };

            Console.WriteLine("Left Join");
            Console.WriteLine(string.Join("\n", leftJoin.Select(e => $"Empleado: {e.NombreEmpleado} Departamento: {e.NombreDepartamento}").ToArray()));
            #endregion

            #region [ Right Join ]
            var rightJoin = from d in departamentos
                            join e in empleados
                            on d.Id equals e.DepartamentoId into enlaceDepartamentoEmpleado
                            from e in enlaceDepartamentoEmpleado.DefaultIfEmpty()
                            select new { NombreDepartamento = d.Nombre, NombreEmpleado = e?.Nombre };

            Console.WriteLine("Right Join");
            Console.WriteLine(string.Join("\n", rightJoin.Select(e => $"Empleado: {e.NombreEmpleado} Departamento: {e.NombreDepartamento}").ToArray()));
            #endregion

            Console.ReadLine();
        }
    }
}
