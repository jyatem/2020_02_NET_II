﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Generics
{
    public class MiArregloGenerico<T>
    {
        private T[] _arreglo;

        public MiArregloGenerico(int longitud)
        {
            _arreglo = new T[longitud + 1];
        }

        public T ObtenerItem(int indice)
        {
            return _arreglo[indice];
        }

        public void ActualizarItem(int indice, T valor)
        {
            _arreglo[indice] = valor;
        }
    }
}
