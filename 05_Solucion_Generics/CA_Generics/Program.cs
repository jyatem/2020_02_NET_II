﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace CA_Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            MiArregloGenerico<int> elementos = new MiArregloGenerico<int>(5);

            for (int i = 0; i < 5; i++)
            {
                elementos.ActualizarItem(i, i * 2);
            }

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(elementos.ObtenerItem(i));
            }

            MiArregloGenerico<string> elementosString = new MiArregloGenerico<string>(2);

            elementosString.ActualizarItem(0, "Jairo");
            elementosString.ActualizarItem(1, "Lina");
        }
    }
}
