﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronRepository.Contratos
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> RetornarDatos();

        void Agregar(T entidad);

        void Eliminar(T entidad);

        void Actualizar(T entidad);
    }
}
