﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolucionEjercicios
{
    public interface IServicioLogin
    {
        void Login(string usuario, string clave);
    }
}
