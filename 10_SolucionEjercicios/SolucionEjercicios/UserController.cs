﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolucionEjercicios
{
    public class UserController : IUserController
    {
        private IServicioMail _servicioMail;
        private IServicioLogin _servicioLogin;

        public UserController(IServicioMail servicioMail, IServicioLogin servicioLogin)
        {
            _servicioMail = servicioMail;
            _servicioLogin = servicioLogin;
        }

        public void Registrar()
        {
            _servicioLogin.Login("usuario", "clave");
            _servicioMail.Enviar("asunto", "mensaje");
        }
    }
}
