﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolucionEjercicios
{
    public interface IServicioMail
    {
        void Enviar(string asunto, string mensaje);
    }
}
