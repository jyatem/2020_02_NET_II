﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CrearFacturaProductoDetalle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DetallesFacturas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        FacturaId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Facturas", t => t.FacturaId, cascadeDelete: true)
                .ForeignKey("dbo.Productos", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.FacturaId)
                .Index(t => t.ProductoId);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaFactura = c.DateTime(nullable: false),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreProducto = c.String(),
                        PrecioUnitario = c.Double(nullable: false),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetallesFacturas", "ProductoId", "dbo.Productos");
            DropForeignKey("dbo.DetallesFacturas", "FacturaId", "dbo.Facturas");
            DropIndex("dbo.DetallesFacturas", new[] { "ProductoId" });
            DropIndex("dbo.DetallesFacturas", new[] { "FacturaId" });
            DropTable("dbo.Productos");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetallesFacturas");
        }
    }
}
