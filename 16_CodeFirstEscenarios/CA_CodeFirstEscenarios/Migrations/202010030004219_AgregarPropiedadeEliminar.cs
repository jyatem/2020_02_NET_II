﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarPropiedadeEliminar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productos", "PropiedadEliminar", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productos", "PropiedadEliminar");
        }
    }
}
