﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmpleadoCedulaUnica : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empleados",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Cedula = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Cedula, unique: true, name: "CedulaUnica");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Empleados", "CedulaUnica");
            DropTable("dbo.Empleados");
        }
    }
}
