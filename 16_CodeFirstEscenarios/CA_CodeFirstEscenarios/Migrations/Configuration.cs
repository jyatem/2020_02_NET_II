﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CA_CodeFirstEscenarios.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(CA_CodeFirstEscenarios.Contexto context)
        {
            context.Paises.AddOrUpdate(
                p => p.NombrePais,
                new Pais { NombrePais = "Colombia", Departamentos = new List<Departamento> { new Departamento { NombreDepartamento = "Antioquia", Ciudades = new List<Ciudad> { new Ciudad { NombreCiudad = "Medellin" }, new Ciudad { NombreCiudad = "Itagui"} } }, new Departamento { NombreDepartamento = "Valle del Cauca" } } },
                new Pais { NombrePais = "Ecuador" }
            );
        }
    }
}
