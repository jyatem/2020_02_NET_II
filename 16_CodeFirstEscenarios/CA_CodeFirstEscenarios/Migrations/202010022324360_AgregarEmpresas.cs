﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarEmpresas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        NIT = c.String(nullable: false, maxLength: 128),
                        CiudadId = c.Int(nullable: false),
                        NombreEmpresa = c.String(),
                    })
                .PrimaryKey(t => new { t.NIT, t.CiudadId })
                .ForeignKey("dbo.Ciudades", t => t.CiudadId)
                .Index(t => t.CiudadId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empresas", "CiudadId", "dbo.Ciudades");
            DropIndex("dbo.Empresas", new[] { "CiudadId" });
            DropTable("dbo.Empresas");
        }
    }
}
