﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CrearCursosEstudiantes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cursos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreCurso = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Estudiantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreEstudiante = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EstudianteCursoes",
                c => new
                    {
                        Estudiante_Id = c.Int(nullable: false),
                        Curso_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Estudiante_Id, t.Curso_Id })
                .ForeignKey("dbo.Estudiantes", t => t.Estudiante_Id, cascadeDelete: true)
                .ForeignKey("dbo.Cursos", t => t.Curso_Id, cascadeDelete: true)
                .Index(t => t.Estudiante_Id)
                .Index(t => t.Curso_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EstudianteCursoes", "Curso_Id", "dbo.Cursos");
            DropForeignKey("dbo.EstudianteCursoes", "Estudiante_Id", "dbo.Estudiantes");
            DropIndex("dbo.EstudianteCursoes", new[] { "Curso_Id" });
            DropIndex("dbo.EstudianteCursoes", new[] { "Estudiante_Id" });
            DropTable("dbo.EstudianteCursoes");
            DropTable("dbo.Estudiantes");
            DropTable("dbo.Cursos");
        }
    }
}
