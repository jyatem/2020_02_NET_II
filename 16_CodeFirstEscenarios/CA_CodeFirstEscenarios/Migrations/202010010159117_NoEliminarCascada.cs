﻿namespace CA_CodeFirstEscenarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NoEliminarCascada : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Ciudades", "DepartamentoId", "dbo.Departamentos");
            DropForeignKey("dbo.Departamentos", "PaisId", "dbo.Paises");
            DropForeignKey("dbo.DetallesFacturas", "FacturaId", "dbo.Facturas");
            DropForeignKey("dbo.DetallesFacturas", "ProductoId", "dbo.Productos");
            AddForeignKey("dbo.Ciudades", "DepartamentoId", "dbo.Departamentos", "Id");
            AddForeignKey("dbo.Departamentos", "PaisId", "dbo.Paises", "Id");
            AddForeignKey("dbo.DetallesFacturas", "FacturaId", "dbo.Facturas", "Id");
            AddForeignKey("dbo.DetallesFacturas", "ProductoId", "dbo.Productos", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetallesFacturas", "ProductoId", "dbo.Productos");
            DropForeignKey("dbo.DetallesFacturas", "FacturaId", "dbo.Facturas");
            DropForeignKey("dbo.Departamentos", "PaisId", "dbo.Paises");
            DropForeignKey("dbo.Ciudades", "DepartamentoId", "dbo.Departamentos");
            AddForeignKey("dbo.DetallesFacturas", "ProductoId", "dbo.Productos", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DetallesFacturas", "FacturaId", "dbo.Facturas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Departamentos", "PaisId", "dbo.Paises", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Ciudades", "DepartamentoId", "dbo.Departamentos", "Id", cascadeDelete: true);
        }
    }
}
