﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Departamentos")]
    public class Departamento
    {
        public int Id { get; set; }

        public string NombreDepartamento { get; set; }

        public int PaisId { get; set; }

        public virtual Pais Pais { get; set; }

        public virtual ICollection<Ciudad> Ciudades { get; set; }
    }
}
