﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Facturas")]
    public class Factura
    {
        public int Id { get; set; }

        public DateTime FechaFactura { get; set; }

        public string Descripcion { get; set; }

        public virtual ICollection<DetalleFactura> DetallesFacturas { get; set; }
    }
}
