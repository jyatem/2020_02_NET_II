﻿using CA_CodeFirstEscenarios.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    public class Contexto: DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<Contexto>(new MigrateDatabaseToLatestVersion<Contexto, Configuration>());
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<Pais> Paises { get; set; }

        public DbSet<Departamento> Departamentos { get; set; }

        public DbSet<Ciudad> Ciudades { get; set; }

        public DbSet<Estudiante> Estudiantes { get; set; }

        public DbSet<Curso> Cursos { get; set; }

        public DbSet<Factura> Facturas { get; set; }

        public DbSet<Producto> Productos { get; set; }

        public DbSet<DetalleFactura> DetallesFacturas { get; set; }

        public DbSet<Empresa> Empresas { get; set; }

        public DbSet<Empleado> Empleados { get; set; }
    }
}
