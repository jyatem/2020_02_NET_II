﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Productos")]
    public class Producto
    {
        public int Id { get; set; }

        public string NombreProducto { get; set; }

        public double PrecioUnitario { get; set; }

        public string Descripcion { get; set; }

        //public string PropiedadEliminar { get; set; }

        public int Stock { get; set; }

        public virtual ICollection<DetalleFactura> DetallesFacturas { get; set; }
    }
}
