﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (Contexto contexto = new Contexto())
                {
                    #region [ Validar eliminar en cascada ]
                    //Pais paisEliminar = contexto.Paises.Find(2);
                    //contexto.Paises.Remove(paisEliminar);
                    //contexto.SaveChanges();
                    #endregion

                    #region [ Llave Compuesta ]
                    //Empresa empresa1 = new Empresa
                    //{
                    //    NIT = "1",
                    //    CiudadId = 3,
                    //    NombreEmpresa = "Empresa 1"
                    //};

                    //Empresa empresa2 = new Empresa
                    //{
                    //    NIT = "2",
                    //    CiudadId = 3,
                    //    NombreEmpresa = "Empresa 2"
                    //};

                    //contexto.Empresas.AddRange(new List<Empresa> { empresa1, empresa2 });
                    //contexto.SaveChanges();
                    #endregion

                    #region [ Validar índice único en DB ]
                    //Empleado empleado1 = new Empleado
                    //{
                    //    Cedula = 10,
                    //    Nombre = "Juan"
                    //};

                    //Empleado empleado2 = new Empleado
                    //{
                    //    Cedula = 20,
                    //    Nombre = "Mauricio"
                    //};

                    //contexto.Empleados.AddRange(new List<Empleado> { empleado1, empleado2 });
                    //contexto.SaveChanges();
                    #endregion

                    #region [ Producto ]
                    Producto producto = new Producto
                    { 
                        NombreProducto = "Camisa",
                        Descripcion = "Adidas",
                        PrecioUnitario = 120000,
                        //PropiedadEliminar = "A eliminar",
                        Stock = 10
                    };

                    contexto.Productos.Add(producto);
                    contexto.SaveChanges();
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
            }
            finally
            {
                Console.WriteLine("Ok");
                Console.ReadLine();
            }
        }
    }
}
