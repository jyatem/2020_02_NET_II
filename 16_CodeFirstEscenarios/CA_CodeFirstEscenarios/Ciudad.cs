﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Ciudades")]
    public class Ciudad
    {
        public int Id { get; set; }

        public string NombreCiudad { get; set; }

        public int DepartamentoId { get; set; }

        public virtual Departamento Departamento { get; set; }

        public virtual ICollection<Empresa> Empresas { get; set; }
    }
}
