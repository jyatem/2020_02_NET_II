﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Cursos")]
    public class Curso
    {
        public int Id { get; set; }

        public string NombreCurso { get; set; }

        public virtual ICollection<Estudiante> Estudiantes { get; set; }
    }
}
