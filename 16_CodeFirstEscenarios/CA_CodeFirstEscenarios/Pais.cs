﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Paises")]
    public class Pais
    {
        public int Id { get; set; }

        public string NombrePais { get; set; }

        public virtual ICollection<Departamento> Departamentos { get; set; }
    }
}
