﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Estudiantes")]
    public class Estudiante
    {
        public int Id { get; set; }

        public string NombreEstudiante { get; set; }

        public virtual ICollection<Curso> Cursos { get; set; }
    }
}
