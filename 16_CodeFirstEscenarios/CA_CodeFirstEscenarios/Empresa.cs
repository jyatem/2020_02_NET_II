﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Empresas")]
    public class Empresa
    {
        [Key]
        [Column(Order = 1)]
        public string NIT { get; set; }

        [Key]
        [Column(Order = 2)]
        public int CiudadId { get; set; }

        public virtual Ciudad Ciudad { get; set; }

        public string NombreEmpresa { get; set; }
    }
}
