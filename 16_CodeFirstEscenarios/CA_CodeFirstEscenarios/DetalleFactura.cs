﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("DetallesFacturas")]
    public class DetalleFactura
    {
        public int Id { get; set; }

        public int Cantidad { get; set; }

        public int FacturaId { get; set; }

        public virtual Factura Factura { get; set; }

        public int ProductoId { get; set; }

        public virtual Producto Producto { get; set; }
    }
}
