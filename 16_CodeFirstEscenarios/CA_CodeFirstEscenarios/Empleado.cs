﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirstEscenarios
{
    [Table("Empleados")]
    public class Empleado
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        [Index("CedulaUnica", IsUnique = true)]
        public int Cedula { get; set; }
    }
}
