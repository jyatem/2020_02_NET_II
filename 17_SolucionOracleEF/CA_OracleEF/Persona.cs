﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_OracleEF
{
    [Table("Personas")]
    public class Persona
    {
        public int Id { get; set; }

        public string NombrePersona { get; set; }

        public virtual ICollection<Pasatiempo> Pasatiempos { get; set; }
    }
}
