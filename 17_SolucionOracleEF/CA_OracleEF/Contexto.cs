﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_OracleEF
{
    public class Contexto : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("YATE");
        }

        public DbSet<Persona> Personas { get; set; }

        public DbSet<Pasatiempo> Pasatiempos { get; set; }
    }
}
