﻿namespace CA_OracleEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DBInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "YATE.Pasatiempos",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        NombrePasatiempo = c.String(),
                        PersonaId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("YATE.Personas", t => t.PersonaId, cascadeDelete: true)
                .Index(t => t.PersonaId);
            
            CreateTable(
                "YATE.Personas",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        NombrePersona = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("YATE.Pasatiempos", "PersonaId", "YATE.Personas");
            DropIndex("YATE.Pasatiempos", new[] { "PersonaId" });
            DropTable("YATE.Personas");
            DropTable("YATE.Pasatiempos");
        }
    }
}
