﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioWebApi.Models;
using ServicioWebApi.Models.DTOs;

namespace ServicioWebApi.Controllers
{
    //[RoutePrefix("api/cursos")]
    public class CursosController : ApiController
    {
        private Contexto db = new Contexto();

        private static readonly Expression<Func<Curso, CursoDTO>> ComoCursoDTO = c => new CursoDTO { Id = c.Id, NombreCurso = c.NombreCurso, NombrePrograma = c.Programa.NombrePrograma };

        public CursosController()
        {
            //db.Configuration.ProxyCreationEnabled = false;
        }

        //[Route("{programa}")]
        //public IQueryable<CursoDTO> GetCursosPorPrograma(string programa)
        //{
        //    var cursos = db.Cursos.Where(c => c.Programa.NombrePrograma.Contains(programa)).Select(ComoCursoDTO);

        //    return cursos;
        //}

        //// GET: api/Cursos
        //public IQueryable<Curso> GetCursos()
        //{
        //    return db.Cursos;
        //    //return db.Cursos.Include(c => c.Programa);
        //}

        // GET: api/Cursos
        public IQueryable<CursoDTO> GetCursos()
        //public IQueryable<CursoDTO> Get()
        //[HttpGet]
        //public IQueryable<CursoDTO> ObtenerCursos()
        {
            //return db.Cursos.Select(c => new CursoDTO { Id = c.Id, NombreCurso = c.NombreCurso, NombrePrograma = c.Programa.NombrePrograma} );
            return db.Cursos.Select(ComoCursoDTO);
        }

        //[HttpGet]
        //public IQueryable<CursoDTO> ObtenerCursosOtro()
        //{
        //    return db.Cursos.Select(c => new CursoDTO { Id = c.Id, NombreCurso = "Otro: " + c.NombreCurso, NombrePrograma = c.Programa.NombrePrograma });
        //}

        //// GET: api/Cursos/5
        //[ResponseType(typeof(Curso))]
        //public IHttpActionResult GetCurso(int id)
        //{
        //    Curso curso = db.Cursos.Find(id);
        //    if (curso == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(curso);
        //}

        // GET: api/Cursos/5
        [ResponseType(typeof(CursoDetalleDTO))]
        public IHttpActionResult GetCurso(int id)
        {
            //Curso curso = db.Cursos.Find(id);
            var curso = db.Cursos.Select(c => new CursoDetalleDTO { Id = c.Id, NombreCurso = c.NombreCurso, NombrePrograma = c.Programa.NombrePrograma, Fecha = c.Fecha }).FirstOrDefault(c => c.Id == id);

            if (curso == null)
            {
                return NotFound();
            }

            return Ok(curso);
        }

        // PUT: api/Cursos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCurso(int id, Curso curso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != curso.Id)
            {
                return BadRequest();
            }

            db.Entry(curso).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CursoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cursos
        [ResponseType(typeof(CursoDetalleDTO))]
        public IHttpActionResult PostCurso(Curso curso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cursos.Add(curso);
            db.SaveChanges();

            db.Entry(curso).Reference(c => c.Programa).Load();

            CursoDetalleDTO cursoDetalleDTO = new CursoDetalleDTO
            {
                Id = curso.Id,
                NombreCurso = curso.NombreCurso,
                Fecha = curso.Fecha,
                NombrePrograma = curso.Programa.NombrePrograma
            };

            return CreatedAtRoute("DefaultApi", new { id = curso.Id }, cursoDetalleDTO);
        }

        // DELETE: api/Cursos/5
        [ResponseType(typeof(Curso))]
        public IHttpActionResult DeleteCurso(int id)
        {
            Curso curso = db.Cursos.Find(id);
            if (curso == null)
            {
                return NotFound();
            }

            db.Cursos.Remove(curso);
            db.SaveChanges();

            return Ok(curso);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CursoExists(int id)
        {
            return db.Cursos.Count(e => e.Id == id) > 0;
        }
    }
}