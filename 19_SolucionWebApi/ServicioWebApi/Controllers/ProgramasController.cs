﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServicioWebApi.Models;
using ServicioWebApi.Models.DTOs;

namespace ServicioWebApi.Controllers
{
    [RoutePrefix("api/programas")]
    public class ProgramasController : ApiController
    {
        private Contexto db = new Contexto();

        private static readonly Expression<Func<Programa, ProgramaDTO>> ComoProgramaDTO = p => new ProgramaDTO { Id = p.Id, NombrePrograma = p.NombrePrograma };

        private static readonly Expression<Func<Curso, CursoDTO>> ComoCursoDTO = c => new CursoDTO { Id = c.Id, NombreCurso = c.NombreCurso, NombrePrograma = c.Programa.NombrePrograma };

        //// GET: api/Programas
        //public IQueryable<Programa> GetProgramas()
        //{
        //    return db.Programas;
        //}

        // GET: api/Programas
        public IQueryable<ProgramaDTO> GetProgramas()
        {
            //throw new Exception("Generando error desde el WebApi");
            return db.Programas.Select(ComoProgramaDTO);
        }

        [Route("~/api/programas/{programaId:int}/cursos")]
        public IQueryable<CursoDTO> GetCursosPorPrograma(int programaId)
        {
            return db.Cursos.Where(c => c.ProgramaId == programaId).Select(ComoCursoDTO);
        }

        // GET: api/Programas/5
        [ResponseType(typeof(ProgramaDTO))]
        public IHttpActionResult GetPrograma(int id)
        {
            Programa programa = db.Programas.Find(id);
            if (programa == null)
            {
                return NotFound();
            }

            ProgramaDTO programaDTO = new ProgramaDTO { Id = programa.Id, NombrePrograma = programa.NombrePrograma };

            return Ok(programaDTO);
        }

        // PUT: api/Programas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPrograma(int id, Programa programa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != programa.Id)
            {
                return BadRequest();
            }

            db.Entry(programa).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgramaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Programas
        [ResponseType(typeof(Programa))]
        public IHttpActionResult PostPrograma(Programa programa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Programas.Add(programa);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = programa.Id }, programa);
        }

        // DELETE: api/Programas/5
        [ResponseType(typeof(Programa))]
        public IHttpActionResult DeletePrograma(int id)
        {
            Programa programa = db.Programas.Find(id);
            if (programa == null)
            {
                return NotFound();
            }

            db.Programas.Remove(programa);
            db.SaveChanges();

            return Ok(programa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgramaExists(int id)
        {
            return db.Programas.Count(e => e.Id == id) > 0;
        }
    }
}