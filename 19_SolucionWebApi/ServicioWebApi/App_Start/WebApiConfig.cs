﻿using ServicioWebApi.Filters;
using ServicioWebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ServicioWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.MessageHandlers.Add(new AutorizacionHandler());

            config.EnableSystemDiagnosticsTracing();
            config.Filters.Add(new ExceptionFilter());

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
