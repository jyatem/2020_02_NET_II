﻿namespace ServicioWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BDInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cursos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreCurso = c.String(nullable: false, maxLength: 80),
                        Fecha = c.DateTime(nullable: false),
                        ProgramaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Programas", t => t.ProgramaId, cascadeDelete: true)
                .Index(t => t.ProgramaId);
            
            CreateTable(
                "dbo.Programas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombrePrograma = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cursos", "ProgramaId", "dbo.Programas");
            DropIndex("dbo.Cursos", new[] { "ProgramaId" });
            DropTable("dbo.Programas");
            DropTable("dbo.Cursos");
        }
    }
}
