﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace ServicioWebApi.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        // Para guardar en base de datos https://stackoverflow.com/questions/9343517/where-does-elmah-save-its-data

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            ErrorLog.GetDefault(HttpContext.Current).Log(new Error(actionExecutedContext.Exception, HttpContext.Current));
        }
    }
}