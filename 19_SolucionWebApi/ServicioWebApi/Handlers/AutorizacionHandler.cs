﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ServicioWebApi.Handlers
{
    public class AutorizacionHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var dominio = "localhost";

            if (!string.Equals(request.RequestUri.Host, dominio, StringComparison.InvariantCultureIgnoreCase))
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            return await base.SendAsync(request, cancellationToken);
        }
    }
}