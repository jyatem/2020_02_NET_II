﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServicioWebApi.Models
{
    [Table("Programas")]
    public class Programa
    {
        public int Id { get; set; }

        [StringLength(100, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 5)]
        [Required(ErrorMessage = "Debe ingresar el campo {0}")]
        public string NombrePrograma { get; set; }

        public virtual ICollection<Curso> Cursos { get; set; }
    }
}