﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicioWebApi.Models.DTOs
{
    public class CursoDTO
    {
        public int Id { get; set; }

        public string NombreCurso { get; set; }

        public string NombrePrograma { get; set; }
    }
}