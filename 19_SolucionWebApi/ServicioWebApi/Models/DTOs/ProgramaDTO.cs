﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicioWebApi.Models.DTOs
{
    public class ProgramaDTO
    {
        public int Id { get; set; }

        public string NombrePrograma { get; set; }
    }
}