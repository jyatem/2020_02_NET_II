﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicioWebApi.Models.DTOs
{
    public class CursoDetalleDTO : CursoDTO
    {
        public DateTime Fecha { get; set; }
    }
}