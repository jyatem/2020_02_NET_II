﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ServicioWebApi.Models
{
    public class Contexto : DbContext
    {
        public DbSet<Programa> Programas { get; set; }

        public DbSet<Curso> Cursos { get; set; }

        public DbSet<Estudiante> Estudiantes { get; set; }
    }
}