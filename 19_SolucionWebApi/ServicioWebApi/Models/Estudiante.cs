﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServicioWebApi.Models
{
    [Table("Estudiantes")]
    public class Estudiante
    {
        public int Id { get; set; }
        
        public string Nombre { get; set; }

        public string Apellidos { get; set; }
    }
}