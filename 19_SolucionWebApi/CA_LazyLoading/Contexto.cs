﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LazyLoading
{
    public class Contexto : DbContext
    {
        public Contexto()
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Programa> Programas { get; set; }

        public DbSet<Curso> Cursos { get; set; }
    }
}
