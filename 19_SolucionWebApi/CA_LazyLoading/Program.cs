﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LazyLoading
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (Contexto contexto = new Contexto())
                {
                    contexto.Database.Log = s => Debug.WriteLine(s);
                    //contexto.Configuration.LazyLoadingEnabled

                    List<Curso> cursos = contexto.Cursos.ToList();
                    //List<Curso> cursos = contexto.Cursos.Include("Programa").ToList();

                    foreach (var item in cursos)
                    {
                        Console.WriteLine(item.Programa.NombrePrograma);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
