﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LazyLoading
{
    [Table("Cursos")]
    public class Curso
    {
        public int Id { get; set; }

        [StringLength(80, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 4)]
        [Required(ErrorMessage = "Debe ingresar el campo {0}")]
        public string NombreCurso { get; set; }

        public DateTime Fecha { get; set; }

        public int ProgramaId { get; set; }

        public virtual Programa Programa { get; set; }
    }
}
