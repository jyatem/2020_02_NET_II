﻿namespace CA_LazyLoading.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CA_LazyLoading.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CA_LazyLoading.Contexto context)
        {
            context.Programas.AddOrUpdate(
                p => p.NombrePrograma,
                new Programa { NombrePrograma = "Ingeniería", Cursos = new List<Curso> { new Curso { NombreCurso = ".NET Básico", Fecha = new DateTime(2020, 10, 14) }, new Curso { NombreCurso = ".NET Avanzado", Fecha = new DateTime(2020, 10, 14) } } }, new Programa { NombrePrograma = "Administracion", Cursos = new List<Curso> { new Curso { NombreCurso = "Gerencia de Proyectos", Fecha = new DateTime(2020, 10, 14) } } }
            );
        }
    }
}
