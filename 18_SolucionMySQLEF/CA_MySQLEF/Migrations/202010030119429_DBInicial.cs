namespace CA_MySQLEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DBInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pasatiempos",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    NombrePasatiempo = c.String(unicode: false),
                    PersonaId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Personas", t => t.PersonaId, cascadeDelete: true);
                //.Index(t => t.PersonaId);
            
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombrePersona = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pasatiempos", "PersonaId", "dbo.Personas");
            DropIndex("dbo.Pasatiempos", new[] { "PersonaId" });
            DropTable("dbo.Personas");
            DropTable("dbo.Pasatiempos");
        }
    }
}
