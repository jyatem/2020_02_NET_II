﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_MySQLEF
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Contexto contexto = new Contexto())
            {
                Persona persona = new Persona
                {
                    NombrePersona = "Jairo",
                    Pasatiempos = new List<Pasatiempo> { new Pasatiempo { NombrePasatiempo = "Xbox" } }
                };

                contexto.Personas.Add(persona);
                contexto.SaveChanges();
            }
        }
    }
}
