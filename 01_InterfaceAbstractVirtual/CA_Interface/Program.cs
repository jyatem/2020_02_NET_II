﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Entrenador entrenador = new Entrenador();
            entrenador.Nombre = "Carlos";
            entrenador.Apellidos = "Queiroz";

            Futbolista futbolista = new Futbolista
            {
                Nombre = "Radamel",
                Apellidos = "Falcao",
                NumeroDorsal = 9
            };

            Imprimir(entrenador);
            Imprimir(futbolista);

            Console.WriteLine("****************************************");
            Console.WriteLine("Número dorsal");
            ImprimirComoFutbolista(futbolista);

            Console.ReadLine();
        }

        static void Imprimir(SeleccionFutbol seleccionFutbol)
        {
            Console.WriteLine($"{seleccionFutbol.Nombre} {seleccionFutbol.Apellidos}");
        }

        static void ImprimirComoFutbolista(SeleccionFutbol seleccionFutbol)
        {
            Futbolista futbolista = seleccionFutbol as Futbolista;
            Console.WriteLine($"{seleccionFutbol.Nombre} {seleccionFutbol.Apellidos} Nro: {futbolista.NumeroDorsal}");
        }
    }
}
