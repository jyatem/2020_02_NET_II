﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Abstract
{
    public class DVD : Producto
    {
        public int NroPistas { get; set; }

        public DVD(string nombreProducto, double precioVenta, double costoFabrica, int nroPistas) : base(nombreProducto, precioVenta, costoFabrica)
        {
            NroPistas = nroPistas;
        }

        public override string ImprimirDatos()
        {
            return $"DVD: {_nombreProducto}, Precio {_precioVenta}, tiene {NroPistas} pistas";
        }
    }
}
