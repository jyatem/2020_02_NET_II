﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Virtual
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Figura> figuras = new List<Figura>();

            figuras.Add(new Rectangulo { Altura = 3, Base = 2 });
            figuras.Add(new Triangulo { Altura = 3, Base = 4 });
            figuras.Add(new Circulo{ Radio = 5 });

            foreach (var figura in figuras)
            {
                Console.WriteLine(figura.CalcularArea());
            }
        }
    }
}
