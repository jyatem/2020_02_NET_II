﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Virtual
{
    public class Triangulo : Figura
    {
        public override double CalcularArea()
        {
            // Revisar cuando veamos SOLID
            return base.CalcularArea() / 2;
        }
    }
}
