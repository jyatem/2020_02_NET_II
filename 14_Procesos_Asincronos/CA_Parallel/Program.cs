﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CA_Parallel
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> valores = new List<string>
            {
                "Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor",
                "Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor",
                "Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor","Valor"
            };

            Parallel.ForEach(valores, valor =>
            {
                Console.WriteLine($"Valor = {valor}, Thread Id = {Thread.CurrentThread.ManagedThreadId}");
            });

            Console.ReadLine();
        }
    }
}
