﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CA_Hilos
{
    public class Tarea
    {
        private string _nombre;
        private int _tiempo;

        public Tarea(string nombre, int tiempo)
        {
            _nombre = nombre;
            _tiempo = tiempo;
        }

        public void EjecutarTarea()
        {
            Console.WriteLine($"Hilo {_nombre} con tiempo de duración {_tiempo}");
            Thread.Sleep(_tiempo * 1000);
            Console.WriteLine($"Hilo {_nombre} terminado");
        }
    }
}
