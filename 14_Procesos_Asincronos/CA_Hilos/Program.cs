﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CA_Hilos
{
    class Program
    {
        static void Main(string[] args)
        {
            Tarea tarea1 = new Tarea("Tarea Nro. 1", 10);
            Tarea tarea2 = new Tarea("Tarea Nro. 2", 5);

            //tarea1.EjecutarTarea();
            //tarea2.EjecutarTarea();

            Thread hilo1 = new Thread(tarea1.EjecutarTarea);
            Thread hilo2 = new Thread(tarea2.EjecutarTarea);

            hilo1.Start();
            hilo2.Start();

            Console.ReadLine();
        }
    }
}
