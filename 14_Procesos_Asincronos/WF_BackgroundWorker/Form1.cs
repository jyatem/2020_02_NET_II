﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_BackgroundWorker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime inicia = DateTime.Now;
            e.Result = "";

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(50);

                backgroundWorker.ReportProgress(i, DateTime.Now);

                if (backgroundWorker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
            }

            TimeSpan duracion = DateTime.Now - inicia;

            e.Result = $"Duración: {duracion.TotalMilliseconds} ms.";
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            DateTime tiempo = Convert.ToDateTime(e.UserState);

            txtResultado.AppendText(tiempo.ToLongTimeString());
            txtResultado.AppendText(Environment.NewLine);
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("La tarea ha sido cancelada");
            }
            else if (e.Error != null)
            {
                MessageBox.Show($"Error: {e.Result}");
            }
            else
            {
                progressBar.Value = 100;
                MessageBox.Show($"La tarea ha sido completada. Resultado {e.Result}");
            }
        }

        private void btnIniciarBW_Click(object sender, EventArgs e)
        {
            backgroundWorker.RunWorkerAsync();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            backgroundWorker.CancelAsync();
        }
    }
}
