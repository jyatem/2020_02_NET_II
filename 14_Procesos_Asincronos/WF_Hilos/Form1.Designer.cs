﻿namespace WF_Hilos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEjecutarHilo = new System.Windows.Forms.Button();
            this.btnMostrarMensaje = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnEjecutarHilo
            // 
            this.btnEjecutarHilo.Location = new System.Drawing.Point(105, 52);
            this.btnEjecutarHilo.Name = "btnEjecutarHilo";
            this.btnEjecutarHilo.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutarHilo.TabIndex = 0;
            this.btnEjecutarHilo.Text = "Ejecutar Hilo";
            this.btnEjecutarHilo.UseVisualStyleBackColor = true;
            this.btnEjecutarHilo.Click += new System.EventHandler(this.btnEjecutarHilo_Click);
            // 
            // btnMostrarMensaje
            // 
            this.btnMostrarMensaje.Location = new System.Drawing.Point(105, 106);
            this.btnMostrarMensaje.Name = "btnMostrarMensaje";
            this.btnMostrarMensaje.Size = new System.Drawing.Size(75, 23);
            this.btnMostrarMensaje.TabIndex = 1;
            this.btnMostrarMensaje.Text = "Mostar Mensaje";
            this.btnMostrarMensaje.UseVisualStyleBackColor = true;
            this.btnMostrarMensaje.Click += new System.EventHandler(this.btnMostrarMensaje_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 282);
            this.Controls.Add(this.btnMostrarMensaje);
            this.Controls.Add(this.btnEjecutarHilo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEjecutarHilo;
        private System.Windows.Forms.Button btnMostrarMensaje;
    }
}

