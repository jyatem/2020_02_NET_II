﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Hilos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEjecutarHilo_Click(object sender, EventArgs e)
        {
            //CorrerHilo();
            Thread hilo = new Thread(CorrerHilo);
            hilo.Start();
        }

        private void btnMostrarMensaje_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Mostrar mensaje");
        }

        private void CorrerHilo()
        {
            Thread.Sleep(5000);
            MessageBox.Show("Proceso finalizado");
        }
    }
}
