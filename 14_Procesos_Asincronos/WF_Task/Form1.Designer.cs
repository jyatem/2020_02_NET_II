﻿namespace WF_Task
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTask = new System.Windows.Forms.Button();
            this.btnVariosTask = new System.Windows.Forms.Button();
            this.btnTaskTipadas = new System.Windows.Forms.Button();
            this.btnTaskSecuenciales = new System.Windows.Forms.Button();
            this.btnSincronizarVariosTask = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTask
            // 
            this.btnTask.Location = new System.Drawing.Point(75, 44);
            this.btnTask.Name = "btnTask";
            this.btnTask.Size = new System.Drawing.Size(228, 23);
            this.btnTask.TabIndex = 0;
            this.btnTask.Text = "Task";
            this.btnTask.UseVisualStyleBackColor = true;
            this.btnTask.Click += new System.EventHandler(this.btnTask_Click);
            // 
            // btnVariosTask
            // 
            this.btnVariosTask.Location = new System.Drawing.Point(75, 92);
            this.btnVariosTask.Name = "btnVariosTask";
            this.btnVariosTask.Size = new System.Drawing.Size(228, 23);
            this.btnVariosTask.TabIndex = 1;
            this.btnVariosTask.Text = "Varios Task";
            this.btnVariosTask.UseVisualStyleBackColor = true;
            this.btnVariosTask.Click += new System.EventHandler(this.btnVariosTask_Click);
            // 
            // btnTaskTipadas
            // 
            this.btnTaskTipadas.Location = new System.Drawing.Point(75, 135);
            this.btnTaskTipadas.Name = "btnTaskTipadas";
            this.btnTaskTipadas.Size = new System.Drawing.Size(228, 23);
            this.btnTaskTipadas.TabIndex = 2;
            this.btnTaskTipadas.Text = "Task Tipadas";
            this.btnTaskTipadas.UseVisualStyleBackColor = true;
            this.btnTaskTipadas.Click += new System.EventHandler(this.btnTaskTipadas_Click);
            // 
            // btnTaskSecuenciales
            // 
            this.btnTaskSecuenciales.Location = new System.Drawing.Point(75, 181);
            this.btnTaskSecuenciales.Name = "btnTaskSecuenciales";
            this.btnTaskSecuenciales.Size = new System.Drawing.Size(228, 23);
            this.btnTaskSecuenciales.TabIndex = 3;
            this.btnTaskSecuenciales.Text = "Task Secuenciales";
            this.btnTaskSecuenciales.UseVisualStyleBackColor = true;
            this.btnTaskSecuenciales.Click += new System.EventHandler(this.btnTaskSecuenciales_Click);
            // 
            // btnSincronizarVariosTask
            // 
            this.btnSincronizarVariosTask.Location = new System.Drawing.Point(75, 229);
            this.btnSincronizarVariosTask.Name = "btnSincronizarVariosTask";
            this.btnSincronizarVariosTask.Size = new System.Drawing.Size(228, 23);
            this.btnSincronizarVariosTask.TabIndex = 4;
            this.btnSincronizarVariosTask.Text = "Sincronizar Varios Task";
            this.btnSincronizarVariosTask.UseVisualStyleBackColor = true;
            this.btnSincronizarVariosTask.Click += new System.EventHandler(this.btnSincronizarVariosTask_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 312);
            this.Controls.Add(this.btnSincronizarVariosTask);
            this.Controls.Add(this.btnTaskSecuenciales);
            this.Controls.Add(this.btnTaskTipadas);
            this.Controls.Add(this.btnVariosTask);
            this.Controls.Add(this.btnTask);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTask;
        private System.Windows.Forms.Button btnVariosTask;
        private System.Windows.Forms.Button btnTaskTipadas;
        private System.Windows.Forms.Button btnTaskSecuenciales;
        private System.Windows.Forms.Button btnSincronizarVariosTask;
    }
}

