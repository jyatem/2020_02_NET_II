﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TareaA()
        {
            Thread.Sleep(2000);
            MessageBox.Show("Se ejecutó la tarea A");
        }

        public void TareaB()
        {
            MessageBox.Show("Se ejecutó la tarea B");
        }

        public void TareaC()
        {
            Thread.Sleep(2000);
            MessageBox.Show("Se ejecutó la tarea C");
        }

        private void btnSincronizarVariosTask_Click(object sender, EventArgs e)
        {
            Task t1 = new Task(TareaA);
            t1.Start();

            Task t2 = new Task(TareaB);
            t2.Start();

            Task t3 = new Task(TareaC);
            t3.Start();

            Task.WaitAll(new Task[] { t1, t2, t3 });

            MessageBox.Show("Se terminó la ejecución de todas las tareas");
        }

        private void btnTaskSecuenciales_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() => {
                TareaA();
            }).ContinueWith((t) => TareaB());
        }

        private void btnTaskTipadas_Click(object sender, EventArgs e)
        {
            Task<int> t = Task.Factory.StartNew(() => 
            {
                return DateTime.Now.Month + 1;
            });
        }

        private void btnVariosTask_Click(object sender, EventArgs e)
        {
            Task task = Task.Factory.StartNew(() =>
            {
                TareaA();
                int resultado = DateTime.Now.Year + 1;
            });

            TareaB();
        }

        private void btnTask_Click(object sender, EventArgs e)
        {
            Task task = new Task(TareaA);
            task.Start();
        }
    }
}
