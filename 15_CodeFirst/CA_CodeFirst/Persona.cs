﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirst
{
    [Table("Personas")]
    public class Persona
    {
        public int PersonaId { get; set; }

        // Si se quiere cambiar el nombre de la llave primaria
        //[Key]
        //public int NumeroIdentificacion { get; set; }

        [StringLength(120)]
        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public bool TieneHijos { get; set; }

        public string Correo { get; set; }

        public bool TieneBono { get; set; }

        public int? Telefono { get; set; }

        [Required]
        public string Direccion { get; set; }

        public string Descripcion { get; set; }

        public double Salario { get; set; }

        public virtual ICollection<Pasatiempo> Pasatiempos { get; set; }
    }
}
