﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirst
{
    [Table("Pasatiempos")]
    public class Pasatiempo
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public int PersonaId { get; set; }

        public virtual Persona Persona { get; set; }
    }
}
