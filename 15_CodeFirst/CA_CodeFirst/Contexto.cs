﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirst
{
    public class Contexto : DbContext
    {
        public DbSet<Persona> Personas { get; set; }

        public DbSet<Articulo> Articulos { get; set; }

        public DbSet<Pasatiempo> Pasatiempos { get; set; }
    }
}
