﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //InsertarPersona();

                using (Contexto contexto = new Contexto())
                {
                    contexto.Database.Log = mensaje => Trace.Write(mensaje);

                    //var registro = contexto.Personas.Where(p => p.Nombre == "Jairo");

                    //foreach (var item in registro)
                    //{
                    //    Console.WriteLine(item.Nombre);
                    //}

                    //foreach (var persona in contexto.Personas.ToList())
                    //{
                    //    Console.WriteLine($"Nombre: {persona.Nombre}");

                    //    List<Pasatiempo> pasatiempos = persona.Pasatiempos.ToList();

                    //    foreach (var pasatiempo in pasatiempos)
                    //    {
                    //        Console.WriteLine($"Pasatiempo: {pasatiempo.Nombre}");
                    //    }
                    //}

                    var innerJoin = from p in contexto.Personas
                                    join t in contexto.Pasatiempos
                                    on p.PersonaId equals t.PersonaId
                                    select new { p.Nombre, Pasatiempo = t.Nombre };

                    foreach (var item in innerJoin)
                    {
                        Console.WriteLine($"{item.Nombre} - {item.Pasatiempo}");
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private static void InsertarPersona()
        {
            using (Contexto contexto = new Contexto())
            {
                Persona persona = new Persona 
                { 
                    Nombre = "Jairo", 
                    Apellidos = "Yate Martinez", 
                    FechaNacimiento = new DateTime(1980, 8, 13), 
                    TieneHijos = true, 
                    Correo = "jyatem@hotmail.com", 
                    Direccion = "Calle 100", 
                    Telefono = 345, 
                    TieneBono = true, 
                    Descripcion = "Descripcion 1", 
                    Salario = 10000000,
                    Pasatiempos = new List<Pasatiempo>()
                };

                persona.Pasatiempos.Add(new Pasatiempo { Nombre = "Fútbol" });

                contexto.Personas.Add(persona);
                contexto.SaveChanges();
            }
        }
    }
}
