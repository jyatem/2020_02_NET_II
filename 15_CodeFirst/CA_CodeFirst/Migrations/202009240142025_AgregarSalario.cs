﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarSalario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tblPersona", "Salario", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tblPersona", "Salario");
        }
    }
}
