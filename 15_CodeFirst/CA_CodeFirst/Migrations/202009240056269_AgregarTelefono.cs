﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarTelefono : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personas", "Telefono", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personas", "Telefono");
        }
    }
}
