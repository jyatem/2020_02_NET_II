﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarDireccion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personas", "Direccion", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personas", "Direccion");
        }
    }
}
