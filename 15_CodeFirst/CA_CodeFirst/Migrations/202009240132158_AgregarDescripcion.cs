﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarDescripcion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personas", "Descripcion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personas", "Descripcion");
        }
    }
}
