﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarCorreo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personas", "Correo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personas", "Correo");
        }
    }
}
