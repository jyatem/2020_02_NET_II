﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarTieneBono : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personas", "TieneBono", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personas", "TieneBono");
        }
    }
}
