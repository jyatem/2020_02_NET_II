﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CA_CodeFirst.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CA_CodeFirst.Contexto context)
        {
            context.Personas.AddOrUpdate(
                p => p.Nombre,
                new Persona { Nombre = "Jairo", Apellidos = "Yate Martinez", FechaNacimiento = new DateTime(1980, 8, 13), TieneHijos = true, Correo = "jyatem@hotmail.com", Direccion = "Calle 100", Telefono = 345, TieneBono = true, Descripcion = "Descripcion 1", Salario = 10000000, Pasatiempos = new List<Pasatiempo> { new Pasatiempo { Nombre = "Fútbol" }, new Pasatiempo { Nombre = "Xbox" } } },
                new Persona { Nombre = "Lina", Apellidos = "Mora Ortiz", FechaNacimiento = new DateTime(1979, 12, 29), TieneHijos = true, Correo = "lmora@hotmail.com", Direccion = "Calle 100", Telefono = 6789, TieneBono = false, Descripcion = "Descripcion 2", Salario = 20000000, Pasatiempos = new List<Pasatiempo> { new Pasatiempo { Nombre = "Netflix"} } }
            );

            context.Articulos.AddOrUpdate(
                a => a.Nombre,
                new Articulo { Nombre = "Pantalla", Stock = 3, PrecioUnitario = 500000, Descripcion = "Pantalla no de Gamer" }
            );
        }
    }
}
