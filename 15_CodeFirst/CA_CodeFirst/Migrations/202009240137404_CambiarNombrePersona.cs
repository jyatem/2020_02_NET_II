﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiarNombrePersona : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Personas", newName: "tblPersona");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.tblPersona", newName: "Personas");
        }
    }
}
