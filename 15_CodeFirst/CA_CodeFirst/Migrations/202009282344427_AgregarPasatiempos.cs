﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarPasatiempos : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.tblPersona", newName: "Personas");
            CreateTable(
                "dbo.Pasatiempos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        PersonaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Personas", t => t.PersonaId, cascadeDelete: true)
                .Index(t => t.PersonaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pasatiempos", "PersonaId", "dbo.Personas");
            DropIndex("dbo.Pasatiempos", new[] { "PersonaId" });
            DropTable("dbo.Pasatiempos");
            RenameTable(name: "dbo.Personas", newName: "tblPersona");
        }
    }
}
