﻿namespace CA_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarTienHijos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personas", "TieneHijos", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personas", "TieneHijos");
        }
    }
}
