﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_CodeFirst
{
    [Table("Articulos")]
    public class Articulo
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        public int Stock { get; set; }

        public decimal PrecioUnitario { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }
    }
}
