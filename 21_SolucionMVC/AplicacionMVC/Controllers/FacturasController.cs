﻿using AplicacionMVC.Models;
using AplicacionMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionMVC.Controllers
{
    public class FacturasController : Controller
    {
        private Contexto db = new Contexto();

        #region [ Acciones para factura ]
        // GET: Facturas
        public ActionResult NuevaFactura()
        {
            FacturaViewModel facturaViewModel = new FacturaViewModel() { Productos = new List<ProductoViewModel>() };

            ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");

            Session["FacturaViewModel"] = facturaViewModel;

            return View(facturaViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NuevaFactura(FacturaViewModel facturaViewModel)
        {
            if (ModelState.IsValid)
            {
                var ingresarFacturaViewModel = Session["FacturaViewModel"] as FacturaViewModel;

                if (ingresarFacturaViewModel.Productos == null || ingresarFacturaViewModel.Productos.Count() == 0)
                {
                    ViewBag.Error = "Debe ingresar productos";

                    ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");

                    return View(facturaViewModel);
                }

                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        var factura = new Factura
                        {
                            PersonaId = facturaViewModel.ClienteId,
                            Fecha = DateTime.Now,
                            DetalleFacturas = ingresarFacturaViewModel.Productos.Select(p => new DetalleFactura { ProductoId = p.ProductoId, Precio = p.Precio, Cantidad = p.Cantidad}).ToList()
                        };

                        db.Facturas.Add(factura);
                        db.SaveChanges();

                        transaccion.Commit();

                        ViewBag.Mensaje = $"La factura Nro. {factura.Id} fue ingresada exitosamente";

                        //Inicializar todas las variables
                        facturaViewModel = new FacturaViewModel() { Productos = new List<ProductoViewModel>() };

                        ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto", "");

                        Session["FacturaViewModel"] = facturaViewModel;

                        return View(facturaViewModel);
                    }
                    catch (Exception ex)
                    {
                        transaccion.Rollback();
                        ViewBag.Error = $"Error: {ex.Message}";

                        ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");

                        return View(facturaViewModel);
                    }
                }
            }

            ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");

            return View(facturaViewModel);
        }

        public ActionResult EliminarItem(int? id)
        {
            var facturaViewModel = Session["FacturaViewModel"] as FacturaViewModel;

            ProductoViewModel productoViewModel = facturaViewModel.Productos.FirstOrDefault(p => p.ProductoId == id);

            if (productoViewModel.Cantidad == 1)
            {
                facturaViewModel.Productos.Remove(productoViewModel);
            }
            else
            {
                productoViewModel.Cantidad--;
            }

            ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");

            return View("NuevaFactura", facturaViewModel);
        }
        #endregion

        #region [ Acciones para producto ]
        // GET
        public ActionResult AgregarProducto(int? ClienteId)
        {
            Session["ClienteId"] = ClienteId;

            ProductoViewModel productoViewModel = new ProductoViewModel();

            ViewBag.ProductoId = new SelectList(db.Productos.OrderBy(p => p.Descripcion), "Id", "Descripcion");

            return View(productoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarProducto(ProductoViewModel productoDetalleFactura)
        {
            if (ModelState.IsValid)
            {
                FacturaViewModel facturaViewModel = Session["FacturaViewModel"] as FacturaViewModel;

                if (!facturaViewModel.Productos.Exists(p => p.ProductoId == productoDetalleFactura.ProductoId))
                {
                    var producto = db.Productos.Find(productoDetalleFactura.ProductoId);

                    productoDetalleFactura.Descripcion = producto.Descripcion;
                    productoDetalleFactura.Precio = producto.Precio;

                    facturaViewModel.Productos.Add(productoDetalleFactura);
                }
                else
                {
                    facturaViewModel.Productos.Find(p => p.ProductoId == productoDetalleFactura.ProductoId).Cantidad += productoDetalleFactura.Cantidad;
                }

                if (Session["ClienteId"] != null)
                {
                    ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto", Session["ClienteId"]);
                }
                else
                {
                    ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");
                }

                return View("NuevaFactura", facturaViewModel);
            }

            ViewBag.ProductoId = new SelectList(db.Productos.OrderBy(p => p.Descripcion), "Id", "Descripcion");

            return View(productoDetalleFactura);

        }

        public ActionResult CancelarAgregarProducto()
        {
            FacturaViewModel facturaViewModel = Session["FacturaViewModel"] as FacturaViewModel;

            if (Session["ClienteId"] != null)
            {
                ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto", Session["ClienteId"]);
            }
            else
            {
                ViewBag.ClienteId = new SelectList(db.Personas.OrderBy(p => p.Nombre).ThenBy(a => a.Apellido), "Id", "NombreCompleto");
            }

            return View("NuevaFactura", facturaViewModel);
        }
        #endregion

        #region [ Sobrecargas ]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}