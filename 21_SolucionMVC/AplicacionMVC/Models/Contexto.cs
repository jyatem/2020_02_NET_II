﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AplicacionMVC.Models
{
    public class Contexto : DbContext
    {
        public Contexto() : base("name=ConexionDB")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<TipoDocumento> TiposDocumento { get; set; }

        public DbSet<Persona> Personas { get; set; }

        public DbSet<Factura> Facturas { get; set; }

        public DbSet<Producto> Productos { get; set; }

        public DbSet<DetalleFactura> DetalleFacturas { get; set; }
    }
}