﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC_Autenticacion.Models;
using MVC_Autenticacion.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Autenticacion.Controllers
{
    public class RolesController : Controller
    {
        private RoleManager<IdentityRole> gestorRoles = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
        
        // GET: Roles
        public ActionResult Index()
        {
            return View(gestorRoles.Roles.Select(r => new RolViewModel { Id = r.Id, Nombre = r.Name}));
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(RolViewModel rolViewModel)
        {
            if (ModelState.IsValid)
            {
                if (!gestorRoles.RoleExists(rolViewModel.Nombre))
                {
                    gestorRoles.Create(new IdentityRole(rolViewModel.Nombre));
                }

                return RedirectToAction("Index");
            }

            return View(rolViewModel);
        }
    }
}