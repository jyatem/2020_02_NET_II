﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC_Autenticacion.Models;
using MVC_Autenticacion.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVC_Autenticacion.Controllers
{
    public class UsuariosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> gestorUsuarios = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(gestorUsuarios.Users.Select(u => new UserViewModel { Email = u.Email, Nombre = u.UserName, Id = u.Id}));
        }

        public ActionResult Roles(string id)
        {
            UserViewModel userViewModel = CargarInformacion(id);
            return View(userViewModel);            
        }

        public ActionResult AgregarRol(string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.Find(userId);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userViewModel = new UserViewModel
            {
                Id = user.Id,
                Nombre = user.UserName,
                Email = user.Email
            };

            ViewBag.RolId = new SelectList(db.Roles.OrderBy(r => r.Name), "Id", "Name");

            return View(userViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarRol(UserViewModel userViewModel)
        {
            var rolId = Request["RolId"];

            if (string.IsNullOrEmpty(rolId))
            {
                ViewBag.RolId = new SelectList(db.Roles.OrderBy(r => r.Name), "Id", "Name");
                return View(userViewModel);
            }

            var rol = db.Roles.Find(rolId);

            if (!gestorUsuarios.IsInRole(userViewModel.Id, rol.Name))
            {
                gestorUsuarios.AddToRole(userViewModel.Id, rol.Name);
            }

            userViewModel = CargarInformacion(userViewModel.Id);
            return View("Roles", userViewModel);
        }

        private UserViewModel CargarInformacion(string userId)
        {
            var usuario = db.Users.Find(userId);

            if (usuario == null) return null;

            UserViewModel userViewModel = new UserViewModel
            {
                Id = usuario.Id,
                Nombre = usuario.UserName,
                Email = usuario.Email,
                Roles = new List<RolViewModel>()
            };

            foreach (var item in usuario.Roles)
            {
                var rol = db.Roles.Find(item.RoleId);

                var rolViewModel = new RolViewModel
                {
                    Id = item.RoleId,
                    Nombre = rol.Name
                };

                userViewModel.Roles.Add(rolViewModel);
            }

            return userViewModel;
        }
    }
}