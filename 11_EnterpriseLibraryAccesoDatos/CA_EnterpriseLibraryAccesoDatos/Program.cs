﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_EnterpriseLibraryAccesoDatos
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseProviderFactory databaseProviderFactory = new DatabaseProviderFactory();
            Database db = databaseProviderFactory.Create("BaseDatos");

            using (IDataReader dataReader = db.ExecuteReader(CommandType.Text, "select * from TblPersona"))
            {
                while (dataReader.Read())
                {
                    Console.WriteLine($"Id: {dataReader["Id"]}, Nombre: {dataReader["Nombre"]}");
                }
            }

            Console.ReadLine();
        }
    }
}
