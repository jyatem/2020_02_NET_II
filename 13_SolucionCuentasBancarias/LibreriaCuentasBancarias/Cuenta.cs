﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaCuentasBancarias
{
    public class Cuenta
    {
        public string Numero { get; set; }

        public double Saldo { get; set; }

        public Cuenta(string numero)
        {
            Numero = numero;
            Saldo = 0;
        }

        public bool Retiro(double valor)
        {
            if (Saldo >= valor)
            {
                Saldo -= valor;
                return true;
            }

            return false;
        }

        public bool Deposito(double valor)
        {
            if (valor > 0)
            {
                Saldo += valor;
                return true;
            }

            return false;
        }

        public double ConsultarSaldo()
        {
            return Saldo;
        }
    }
}
