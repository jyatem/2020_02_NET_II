﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaCuentasBancarias
{
    public class Banco
    {
        public List<Cuenta> Cuentas { get; set; }

        public Banco()
        {
            Cuentas = new List<Cuenta>();
        }

        // Implementar los siguientes métodos
        public bool ExisteCuenta(string numero)
        {
            return Cuentas.Any(x => x.Numero.Equals(numero));
        }

        public bool AgregarCuenta(string numero)
        {
            if (!ExisteCuenta(numero))
            {
                Cuentas.Add(new Cuenta(numero));
            }

            return false;
        }
        
        public Cuenta BuscarCuenta(string numero)
        {
            if (ExisteCuenta(numero))
            {
                return Cuentas.FirstOrDefault(c => c.Numero.Equals(numero));
            }

            return null;
        }
        
        public bool Transferencia(Cuenta origen, Cuenta destino, double valor)
        {
            if (origen.Retiro(valor))
            {
                destino.Deposito(valor);
                return true;
            }

            return false;
        }
    }
}
