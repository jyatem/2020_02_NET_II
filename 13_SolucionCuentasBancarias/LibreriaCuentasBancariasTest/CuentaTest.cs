﻿using System;
using LibreriaCuentasBancarias;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibreriaCuentasBancariasTest
{
    [TestClass]
    public class CuentaTest
    {
        Cuenta cuenta;

        [TestInitialize]
        public void Iniciar()
        {
            cuenta = new Cuenta("123456789");
        }
        
        [TestMethod]
        public void IniciaSaldoEnCero()
        {
            Cuenta cuentaIniciaSaldoEnCero = new Cuenta("09876");
            Assert.AreEqual(0, cuentaIniciaSaldoEnCero.Saldo);
        }

        [TestMethod]
        public void HacerDepositoMayorQueCero()
        {
            bool realizoDeposito = cuenta.Deposito(100);
            Assert.IsTrue(realizoDeposito);
        }
    }
}
