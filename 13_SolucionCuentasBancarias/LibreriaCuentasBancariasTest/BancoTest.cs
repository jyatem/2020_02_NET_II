﻿using System;
using LibreriaCuentasBancarias;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibreriaCuentasBancariasTest
{
    [TestClass]
    public class BancoTest
    {
        Banco banco;

        [TestInitialize]
        public void Iniciar()
        {
            banco = new Banco();
        }

        [TestMethod]
        public void NoExisteCuenta()
        {
            bool noExiste = banco.ExisteCuenta("12345");
            Assert.IsFalse(noExiste);
        }

        [TestMethod]
        public void BuscarCuentaExiste()
        {
            banco.AgregarCuenta("45678");

            Cuenta cuenta = banco.BuscarCuenta("45678");

            Assert.IsNotNull(cuenta);
        }
    }
}
