﻿using AccesoDatos;
using AccesoDatos.Contratos;
using AccesoDatos.SQLServer;
using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace FacturacionConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Agregar Unity para habilitar la inyección de dependencias
                // Registrar los tipos que se requieran inyectar
                IUnityContainer unityContainer = new UnityContainer();

                unityContainer.RegisterType<Contexto, ContextoSQLServer>();
                unityContainer.RegisterType<IAccesoDatosCliente, AccesoDatosCliente>();

                // Instanciar FachadaCliente e insertar un cliente
                FachadaCliente fachadaCliente = unityContainer.Resolve<FachadaCliente>();

                //Cliente cliente = new Cliente
                //{
                //    NombreCliente = "Lina"
                //};

                //fachadaCliente.InsertarCliente(cliente);

                // Si todo salió bien, se debe ver creada la base de datos, con su tabla cliente y el registro

                Cliente cliente = new Cliente
                {
                    Cedula = Convert.ToInt32(DateTime.Now.ToString("yyyyhhmmss")),
                    NombreCliente = "Mariana",
                    FechaNacimiento = new DateTime(1980, 8, 13)
                };

                fachadaCliente.InsertarCliente(cliente);

                cliente.NombreCliente = "Mariana Yate";
                fachadaCliente.ActualizarCliente(cliente);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Ok");
                Console.ReadLine();
            }
        }
    }
}
