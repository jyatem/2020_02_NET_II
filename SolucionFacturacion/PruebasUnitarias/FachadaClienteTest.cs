﻿using System;
using AccesoDatos.Contratos;
using LogicaNegocio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modelos;
using Moq;

namespace PruebasUnitarias
{
    [TestClass]
    public class FachadaClienteTest
    {
        private Mock<IAccesoDatosCliente> _mockAccesoDatosCliente;
        private FachadaCliente _fachadaCliente;

        [TestInitialize]
        public void Inicializar()
        {
            _mockAccesoDatosCliente = new Mock<IAccesoDatosCliente>();

            _mockAccesoDatosCliente.Setup(c => c.InsertarCliente(It.IsAny<Cliente>())).Returns(1);

            _fachadaCliente = new FachadaCliente(_mockAccesoDatosCliente.Object);
        }

        [TestMethod]
        public void InsertarClienteMayorDeEdad()
        {
            Cliente cliente = new Cliente
            {
                NombreCliente = "Jairo",
                FechaNacimiento = new DateTime(1980, 8, 13)
            };

            Assert.IsTrue(_fachadaCliente.InsertarCliente(cliente));
        }

        [TestMethod]
        public void InsertarClienteMenorDeEdad()
        {
            Cliente cliente = new Cliente
            {
                NombreCliente = "Mariana",
                FechaNacimiento = new DateTime(2006, 2, 14)
            };

            Assert.IsFalse(_fachadaCliente.InsertarCliente(cliente));
        }
    }
}
