﻿using System;
using System.Threading.Tasks;
using AccesoDatos;
using AccesoDatos.Contratos;
using AccesoDatos.SQLServer;
using LogicaNegocio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modelos;
using Unity;

namespace PruebasIntegracion
{
    [TestClass]
    public class FachadaClienteTestIntegracion
    {
        IUnityContainer _unityContainer;
        FachadaCliente _fachadaCliente;

        [TestInitialize]
        public void Inicializar()
        {
            _unityContainer = new UnityContainer();

            _unityContainer.RegisterType<Contexto, ContextoSQLServer>();
            _unityContainer.RegisterType<IAccesoDatosCliente, AccesoDatosCliente>();

            _fachadaCliente = _unityContainer.Resolve<FachadaCliente>();
        }

        [TestMethod]
        public void InsertarCliente()
        {
            Cliente cliente = new Cliente
            {
                Cedula = Convert.ToInt32(DateTime.Now.ToString("yyyyhhmmss")),
                NombreCliente = "Jairo",
                FechaNacimiento = new DateTime(1980, 8, 13)
            };

            _fachadaCliente.InsertarCliente(cliente);

            Cliente clienteIngresado = _fachadaCliente.ObtenerCliente(cliente.Cedula);

            Assert.AreEqual(cliente, clienteIngresado);
        }

        [TestMethod]
        public void ActualizarClienteExitosamente()
        {
            Cliente cliente = new Cliente
            {
                Cedula = Convert.ToInt32(DateTime.Now.ToString("yyyyhhmmss")),
                NombreCliente = "Lina",
                FechaNacimiento = new DateTime(1980, 8, 13)
            };

            _fachadaCliente.InsertarCliente(cliente);

            cliente.NombreCliente = "Lina Mora";
            _fachadaCliente.ActualizarCliente(cliente);

            Cliente clienteActualizado = _fachadaCliente.ObtenerCliente(cliente.Cedula);

            Assert.AreEqual(cliente, clienteActualizado);
        }

        [TestMethod]
        public async Task EliminarClienteAsyncExitosamente()
        {
            Cliente cliente = new Cliente
            {
                Cedula = Convert.ToInt32(DateTime.Now.ToString("yyyyhhmmss")),
                NombreCliente = "Maria",
                FechaNacimiento = new DateTime(1980, 8, 13)
            };

            _fachadaCliente.InsertarCliente(cliente);

            Cliente clienteEliminar = _fachadaCliente.ObtenerCliente(cliente.Cedula);

            Assert.IsTrue(await _fachadaCliente.EliminarClienteAsync(clienteEliminar.Id));
        }
    }
}
