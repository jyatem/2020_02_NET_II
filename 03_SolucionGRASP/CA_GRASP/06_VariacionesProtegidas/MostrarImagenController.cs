﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_GRASP._06_VariacionesProtegidas
{
    public class MostrarImagenController
    {
        // Usaría una interfaz en lugar del objeto concreto
        public void RedimensionarImagen(ImagenJpeg imagenJpeg)
        {
            imagenJpeg.Redimensionar(20, 10);
        }
    }
}
