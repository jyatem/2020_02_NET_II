﻿using AccesoDatos.Contratos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaPersona
    {
        // Este código tiene inversión de dependencias

        private IAccesoDatosPersona _accesoDatosPersona;

        public FachadaPersona(IAccesoDatosPersona accesoDatosPersona)
        {
            _accesoDatosPersona = accesoDatosPersona;
        }

        public void InsertarPersona(Persona persona)
        {
            // Logica de negocio que valide a persona...
            _accesoDatosPersona.Insertar(persona);
        }
    }
}
