﻿using AccesoDatos.Contratos;
using AccesoDatos.Oracle;
using AccesoDatos.SQLServer;
using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace CA_Cliente
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnityContainer unityContainer = new UnityContainer();

            //unityContainer.RegisterType<IAccesoDatosPersona, AccesoDatosPersonaSQLServer>();
            unityContainer.RegisterType<IAccesoDatosPersona, AccesoDatosPersonaOracle>();

            FachadaPersona fachadaPersona = unityContainer.Resolve<FachadaPersona>();

            fachadaPersona.InsertarPersona(new Persona { Id = 1, Nombre = "Jairo", Apellidos = "Yate" });
        }
    }
}
