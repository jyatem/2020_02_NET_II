﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA_Lambda
{
    class Program
    {
        delegate bool EsAdolescente(Persona persona);

        static void Main(string[] args)
        {
            Persona persona = new Persona { Nombre = "Mariana", Edad = 13 };

            EsAdolescente esAdolescente = delegate (Persona p) { return p.Edad > 10 && p.Edad <= 18; };

            Console.WriteLine($"{persona.Nombre} es {esAdolescente(persona)}");

            EsAdolescente esAdolescenteLambda = p => p.Edad > 10 && p.Edad <= 18;

            Console.WriteLine($"{persona.Nombre} es {esAdolescenteLambda(persona)}");
        }
    }
}
