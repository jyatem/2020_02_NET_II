﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA_Lambda
{
    public class Persona
    {
        public string Nombre { get; set; }

        public int Edad { get; set; }
    }
}
