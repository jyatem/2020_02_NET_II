﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Lambda
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            btnDelegate.Click += delegate (object sender, EventArgs e)
            {
                MessageBox.Show("Desde un delegado");
            };

            btnLambda.Click += (sender, e) => MessageBox.Show("Desde Lambda ");
        }

        private void btnEventHandler_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Desde event handler");
        }
    }
}
