﻿namespace WF_Lambda
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEventHandler = new System.Windows.Forms.Button();
            this.btnDelegate = new System.Windows.Forms.Button();
            this.btnLambda = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnEventHandler
            // 
            this.btnEventHandler.Location = new System.Drawing.Point(54, 57);
            this.btnEventHandler.Name = "btnEventHandler";
            this.btnEventHandler.Size = new System.Drawing.Size(146, 23);
            this.btnEventHandler.TabIndex = 0;
            this.btnEventHandler.Text = "Event Handler";
            this.btnEventHandler.UseVisualStyleBackColor = true;
            this.btnEventHandler.Click += new System.EventHandler(this.btnEventHandler_Click);
            // 
            // btnDelegate
            // 
            this.btnDelegate.Location = new System.Drawing.Point(54, 103);
            this.btnDelegate.Name = "btnDelegate";
            this.btnDelegate.Size = new System.Drawing.Size(146, 23);
            this.btnDelegate.TabIndex = 1;
            this.btnDelegate.Text = "Delegate";
            this.btnDelegate.UseVisualStyleBackColor = true;
            // 
            // btnLambda
            // 
            this.btnLambda.Location = new System.Drawing.Point(54, 149);
            this.btnLambda.Name = "btnLambda";
            this.btnLambda.Size = new System.Drawing.Size(146, 23);
            this.btnLambda.TabIndex = 2;
            this.btnLambda.Text = "Lambda";
            this.btnLambda.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 267);
            this.Controls.Add(this.btnLambda);
            this.Controls.Add(this.btnDelegate);
            this.Controls.Add(this.btnEventHandler);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEventHandler;
        private System.Windows.Forms.Button btnDelegate;
        private System.Windows.Forms.Button btnLambda;
    }
}

